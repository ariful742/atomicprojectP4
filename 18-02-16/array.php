<?php

include_once 'debug.php';
$arr = array(
   'false' => "One",
    '0.05' => "Two",
    '5a' => "Three"
);

debug($arr)."</br>";

$arr2 = array(
   '1' => "One",
    'one' => "Two",
     "Three"
);

debug($arr2)."</br>";

$arr3 = array(
   '10' => "One",
    '5' => "Two",
    "Two",
    '25' => "Three"
);

debug($arr3)."</br>";
